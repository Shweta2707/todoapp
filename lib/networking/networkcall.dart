
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;

class NetworkCall{
  NetworkCall._();

  static const String BASE_URL ='https://jsonplaceholder.typicode.com/todos';



  static Future<String> getRequest({
    BuildContext context,
    Map<String,String> header,
    @required Function onSuccess,
    @required Function onError,
  })async{

    try{
      var response = await http.get(BASE_URL,headers: header);
      if(response.statusCode==200 || response.statusCode==201){
        onSuccess(response.body);
      }else{
        onError(response.body);
        print(response.statusCode);
      }
    }catch(e){
      print(e);
      onError('response.body');
    }
  }
}
