import 'dart:io';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:path_provider/path_provider.dart';
import 'package:provider/provider.dart'as pathProvider;
import 'package:provider/provider.dart';
import 'package:todo_app/hive/hive_Db.dart';
import 'package:todo_app/models/todoListModel.dart';
import 'package:todo_app/providers/todoListProvider.dart';
import 'package:todo_app/utility/utility.dart';

class TodoListScreen extends StatefulWidget {
  @override
  _TodoListScreenState createState() => _TodoListScreenState();
}

class _TodoListScreenState extends State<TodoListScreen> {
  final HiveDbHelper dbHelper = HiveDbHelper();
  final scaffoldKey = GlobalKey<ScaffoldState>();

  @override
   initState()  {
    super.initState();
      checkInternetConnectivity();
  }

  checkInternetConnectivity() async{
    var result = await Connectivity().checkConnectivity();
    if(result == ConnectivityResult.none){
      Utility.showSnackBar("No Internet Connection", scaffoldKey);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: scaffoldKey,
      backgroundColor: Colors.grey[200],
      appBar: AppBar(
        title: Text("Todo List"),
      ),
      body: loadTodoList()
    );
  }

  Widget loadTodoList(){
    return FutureBuilder<List<TodoListModel>>(
      future: Provider.of<TodoListProvider>(context).getTodoList(),
      builder: (context,snapshot){
        if(snapshot.connectionState != ConnectionState.done){
          return Center(
            child: CircularProgressIndicator(),
          );
        }
        else if (snapshot.hasData){
          return ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context,int index){
                return Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Card(
                    elevation: 2.0,
                    color:Provider.of<TodoListProvider>(context).getColor(snapshot.data[index].completed) ,
                    child: ListTile(
                      title: Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: Text(snapshot.data[index].title),
                      ),
                      subtitle: (snapshot.data[index].completed == true)?Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: Text("Completed",),
                      ):Padding(
                        padding: const EdgeInsets.all(6.0),
                        child: Text("Remaining"),
                      )
                    ),
                  ),
                );
              }
          );
        }
        return Container();
      },

    );
  }


}
