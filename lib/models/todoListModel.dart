// To parse this JSON data, do
//
//     final todoListModel = todoListModelFromJson(jsonString);

import 'dart:convert';
import 'package:hive/hive.dart';
part 'todoListModel.g.dart';

List<TodoListModel> todoListModelFromJson(String str) => List<TodoListModel>.from(json.decode(str).map((x) => TodoListModel.fromJson(x)));

String todoListModelToJson(List<TodoListModel> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

@HiveType(typeId: 0)
class TodoListModel {
  TodoListModel({
    this.userId,
    this.id,
    this.title,
    this.completed,
  });
@HiveField(0)
  int userId;
@HiveField(1)
  int id;
@HiveField(2)
  String title;
@HiveField(3)
  bool completed;

  factory TodoListModel.fromJson(Map<String, dynamic> json) => TodoListModel(
    userId: json["userId"],
    id: json["id"],
    title: json["title"],
    completed: json["completed"],
  );

  Map<String, dynamic> toJson() => {
    "userId": userId,
    "id": id,
    "title": title,
    "completed": completed,
  };
}
