
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:todo_app/hive/hive_Db.dart';
import 'package:todo_app/models/todoListModel.dart';
import 'package:todo_app/networking/networkcall.dart';

class TodoListProvider extends ChangeNotifier{

  List<TodoListModel> todoListModel = [];
  final HiveDbHelper dbHelper = HiveDbHelper();
  bool loading = true;

  Map<String,String> requestHeaders = {
    'accept': 'application/json',
    'content-type':'application/json',
  };

  Future<List<TodoListModel>> getTodoList() async{
    await NetworkCall.getRequest(
      header: requestHeaders,
        onSuccess: (response){
        if(response!= null){
          var data = json.decode(response);
          todoListModel = todoListModelFromJson(response);
          loading = false;
          dbHelper.addList(todoListModel);
        }
        },
        onError: (error){
          print(error);
          loading=false;
        });
    return todoListModel;
  }

  Color getColor( bool number){
    if(number == true){
      return Colors.lightGreenAccent;
    }
  }

}