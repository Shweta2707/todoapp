
import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';

class Utility {

  static showSnackBar(String message,GlobalKey<ScaffoldState> scaffold){
    scaffold.currentState.showSnackBar(
      SnackBar(
          content: Text(message ?? 'Something went wrong'),
        duration: Duration(seconds: 2),
        behavior: SnackBarBehavior.floating,
      )
    );
  }




}