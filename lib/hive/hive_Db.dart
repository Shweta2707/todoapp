
import 'package:hive/hive.dart';
import 'package:todo_app/models/todoListModel.dart';

class HiveDbHelper {

//method to add todolist to hive
  addList(List<TodoListModel> todoList) async {
    var list = await Hive.openBox('todoBox');
    var result = await list.addAll(todoList);
    return result;
  }


//to fetch all todolist from hive
  Future<List<TodoListModel>> getAllTodoList() async {
    List<TodoListModel> finalList = [];
    List<TodoListModel> model = [];

    final result = await Hive.openBox('todoBox');
    if(result.length == 0){
      return finalList;
    }else{
      for(var i = 0;i < result.length;i++){
        final data = result.getAt(i);
         model = todoListModelFromJson(data);
        finalList.addAll(model);
      }
      return finalList;
    }
  }

}